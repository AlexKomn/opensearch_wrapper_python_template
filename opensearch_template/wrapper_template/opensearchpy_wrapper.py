import functools

from opensearchpy import OpenSearch, helpers
from opensearchpy.helpers import bulk, scan
from opensearchpy.exceptions import NotFoundError, RequestError, ConnectionError

from common.logger import Logger

logger = Logger(__name__)

def handle_opensearch_errors(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except NotFoundError as e: 
            logger.error(f"Index or document not found: {e}")
        except RequestError as e: 
            logger.error(f"Invalid request: {e}")
        except ConnectionError as e: 
            logger.error(f"Connection falied: {e}")
        except Exception as e: 
            logger.error(f"General exception: {e}")
    return wrapper

class OpenSearchConnection:
    def __init__(self, host, port, username, password):
        self.host = host 
        self.port = port
        self.username = username
        self.password = password
        self.client = self.connect_to_opensearch()
        if self.client.ping():
            logger.info("Connected to OpenSearch")
    
    @handle_opensearch_errors
    def connect_to_opensearch(self):
        return OpenSearch(
            hosts=[{"host": self.host, "port": self.port}],
            http_compress = True,
            http_auth = (self.username, self.password),
            use_ssl = True,
            verify_certs = False,
            ssl_assert_hostname = False,
            ssl_show_warn = False
        )
       

class IndexManager:
    def __init__(self, connection: OpenSearchConnection):
        self.connection = connection
    
    @handle_opensearch_errors
    def create_index(self, index_name: str, mapping: dict | None = None):
        body = {}
        if mapping:
            body = {'mappings': {'properties': mapping}} 
        response = self.connection.client.indices.create(
            index=index_name, 
            body=body
        )
        logger.info(f'Created index: {index_name}')
        return response

    @handle_opensearch_errors
    def delete_index(self, index_name: str):
        self.connection.client.indices.delete(index=index_name)
        logger.warning(f'Deleted index {index_name}')
        return True       
    
    @handle_opensearch_errors
    def list_indices(self):
        response = self.connection.client.cat.indices(v=True)
        logger.debug("List of indices\n" + response)
        return response
         

class DocumentManager:
    def __init__(self, connection):
        self.client = connection

    @handle_opensearch_errors
    def get_document_by_id(self, index_name: str, doc_id):
        response = self.client.get(index_name, id=doc_id)
        logger.info(f"Getting document from index: {index_name} with id: {doc_id}")
        return response['_source']
    
    @handle_opensearch_errors
    def insert_document(self, index_name, body, doc_id = None):
        response = self.client.indices.create(index=index_name, id=doc_id, body=body)
        logger.info(f'Insert one document to {index_name} with body: {body}')
        return response

    @handle_opensearch_errors
    def update_document_by_id(self, index_name: str, doc_id, body):
        response = self.client.update(index=index_name, id=doc_id, body=body)
        logger.info(str(response))
        return True
    
    @handle_opensearch_errors
    def delete_document(self, index_name: str, doc_id):
        response = self.client.delete(index=index_name, id=doc_id)
        logger.info(f'Deleted document {doc_id} from {index_name} index')
        return True
    
    @handle_opensearch_errors
    def search(self, query: dict, index_name: str):
        result = self.es.search(
            index=index_name,
            body={
                'query': query
            }
        )
        logger.info(f'Search executed on index {index_name} with query {query}')

    @handle_opensearch_errors
    def bulk_documents(self, index_name: str, documents):
        actions = [
            {
                '_index': index_name,
                '_source': doc

            }
            for doc in documents
        ]
        response = bulk(self.client, actions)
        logger.info(f"Bulk insert {str(response)} documents to {index_name} index")
    
    @handle_opensearch_errors
    def scan_documents(self, index_name: str):
        response = scan(self.client, index=index_name)
        for doc in response:
            yield doc['_source']
    
    @handle_opensearch_errors
    def count(self, index_name: str):
        self.client.indices.refresh(index=index_name)
        result = self.client.count(index=index_name)
        logger.info(f'Count of {index_name} index is: {result["count"]}')
        return result["count"]
    
    @handle_opensearch_errors
    def delete_by_query(self, query: dict, index_name: str):
        self.client.delete_by_query(
            index=index_name,
            body = {
                'query': query
            }
        )
        logger.warning(f'Deleted from index {index_name} using query {query}')
    
    def info(self):
        cluster_info = self.client.info()
        logger.debug(cluster_info)
        return cluster_info