import logging
green : str = '\033[1;32m%s\033[00m'
yellow : str = '\033[33m%s\033[00m'
red : str = '\033[31m%s\033[00m'


class Logger:
    def __init__(self, name: str, log_level="INFO"):
        self.name = name
        self.log_level = log_level

        self.logger = logging.getLogger(name)
        if not self.logger.handlers:
            self.logger.setLevel(self.log_level)
            self.logger.propagate = False

            formatter = logging.Formatter("[%(levelname)s] %(asctime)s - %(name)s - %(funcName)s:%(lineno)d - %(message)s")
            # log to console
            console_handler = logging.StreamHandler()
            console_handler.setLevel(self.log_level)
            console_handler.setFormatter(formatter)
            self.logger.addHandler(console_handler)
    
    def debug(self, msg):
        self.logger.debug(msg)
        
    def info(self, msg):
        self.logger.info(green % msg)

    def warning(self, msg):
        self.logger.warning(yellow % msg)

    def error(self, msg):
        self.logger.error(red % msg)
