import argparse
from decouple import config
from common.logger import Logger
from wrapper_template.opensearchpy_wrapper import OpenSearchConnection, IndexManager

_logger = Logger(__name__)

def main():
    pass
    


if __name__ == "__main__":
    global_parser = argparse.ArgumentParser(prog="My OpenSearch CLI tool", 
                                            description="Solely solves my problems with OpenSearch. Fuck it's my learn by doing program why are you are reading description?",
                                            epilog="Tell how miser I am to alexius.komnin@gmail.com")
    global_parser.add_argument("--host", default="localhost", help="Opensearch host DNS name or IP. By default is %(default)s. You don't really want to use it with your production")
    global_parser.add_argument("-p", "--port", default="9200", help="Opensearch port. By default is %(default)s. Seriously you have something that is not 9200?")
    global_parser.add_argument("-P", "--password", help="Opensearch password. Must be provided. A new shit since 2.x version. I didn't planned to change this to optional until I have to", required=True)
    global_parser.add_argument("-v", "--verbose", action="store_true" , help="Turn on Debug log messages. Yep, pray that I left any relevant data in debug messages")

    subparsers = global_parser.add_subparsers(title="Available Commands", dest="commands", required=True)
    index_parser = subparsers.add_parser('index', help="OpenSearch index operation")
    document_parser = subparsers.add_parser('document', help="OpenSearch operations on document")
    indices_list_parser = subparsers.add_parser('indices', help="OpenSearch list all indices")

    index_subparser = index_parser.add_subparsers(title="Available commands", dest="index")
    index_parser.add_argument("--index-name", help="Name of the index", required=True)
    index_create_parser = index_subparser.add_parser('create', help="Create index")
    index_create_parser.add_argument("--mapping", help="A mapping for index")
    index_delete_parser = index_subparser.add_parser('delete', help="Delete index")
    
    #print(indices_list_parser.parse_args())    


    print(global_parser.parse_args())
    args = global_parser.parse_args()
    opensearch_client = OpenSearchConnection(
        host=args.host,
        port=args.port,
        username="admin",
        password=args.password
    )

    if args.verbose:
        _logger.log_level = "DEBUG"

    match args.commands:
        case "index":
            index_manager = IndexManager(opensearch_client)
            match args.index:
                case "create":
                    if args.mapping:
                        index_manager.create_index(index_name=args.index_name, mapping=args.mapping)
                    index_manager.create_index(index_name=args.index_name)
                case "delete":
                    index_manager.delete_index(index_name=args.index_name)
                
        case "indices":
            index_manager = IndexManager(opensearch_client)
            print(index_manager.list_indices())
            exit(0)
    
    main()